package br.com.resistence.util;

public class Messages {
	
	public static final String ITEMS_ARMA = "Arma";
	public static final String ITEMS_MUNICAO = "Municao";
	public static final String ITEMS_COMIDA = "Comida";
	public static final String ITEMS_AGUA = "Agua";
	
	
	
	public static final String REBEL_GENRE = "Gênero inválido. ex: F ou M";
	
	public static final String REBEL_TYPE_NAME = "Rebelde";
	
	public static final boolean REBEL_TYPE_REBEL = true;
	public static final boolean REBEL_TYPE_LEADER = false;
	
	public static final boolean REBEL_TRAITOR_OFF = false;
	public static final boolean REBEL_TRAITOR_ON = true;
	
	public static final String MESSAGES_CONFLICT_REGISTER = "Apenas líderes podem registrar	rebeldes";
	public static final String MESSAGES_REBEL_DUPLICATE = "Rebelde já cadastrado.";
	
	public static final String MESSAGES_BAD_REQUEST_LOCATION = "Campos de localização não pode ser nulo";
	public static final String MESSAGES_BAD_REQUEST_AGE = "Não permitido rebeldes menores de idade";
	public static final String MESSAGES_BAD_REQUEST = "Campo inválido";
	
	public static final String REBEL_NOT_FOUND = "Rebelde inexistente";
	public static final String REBEL_NOT_FOUND_TRAITOR = "Rebelde traidor inexistente";
	public static final String REBEL_CONFLICT_VOTO = "Rebelde já votou nesse traidor";
	
	public static final String NEGOTIATION_NOT_FOUND = "Rebelde inexistente";
	public static final String NEGOTIATION_CONFLICT_POINT = "Pontos incopatível para troca";
	
	public static final String LEADER_BAD_REQUEST_PSWD = "Senha inválida";
	public static final String LEADER_CONFLICT_REPEATED = "CPF já cadastrado ";
	public static final String LEADER_NOT_FOUND = "Líder inexistente ";
	
	public static final String BAD_REQUEST_CPF = "CPF inválido";
	
}
