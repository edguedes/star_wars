package br.com.resistence.util;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.Random;

import br.com.resistence.exception.BadRequestException;
import br.com.resistence.leader.model.Leader;
import br.com.resistence.location.dto.LocationDTO;

public class Util {

	private static String genero[] = {"M", "F" };

	private static String itens[] = {Messages.ITEMS_ARMA, Messages.ITEMS_MUNICAO,
									Messages.ITEMS_COMIDA, Messages.ITEMS_AGUA};

	public static boolean isValidateGenero(String sexo) {
		return Arrays.stream(genero).anyMatch(sexo::equals);
	}

	public static boolean isValidateItens(String utensilio) {
		return Arrays.stream(itens).anyMatch(utensilio::equals);
	}

	public static int valueItems(String valor) {

		switch (valor) {
		case "Arma":
			return 4;

		case "Municao":
			return 3;

		case "Agua":
			return 2;
		default:
			return 1;
		}         
	}

	public static String removeSpecialCharacters(String input) {
		String inputAscRemove = Normalizer.normalize(input, Normalizer.Form.NFD)
				.replaceAll("[^ a-zA-Z0-9_-]", "").trim();
		return inputAscRemove;
	}

	public static int numberRandon() {
		Random random = new Random();
		int result = random.nextInt(100000000);

		return result;
	}
	
	public static Long validateAge(Long idade) {
		if (idade < 18) {
			throw new BadRequestException(Messages.MESSAGES_BAD_REQUEST_AGE);
		}
		return idade;
	}
	
	public static void validateString(String texto) {
		if ("".equals(texto) || texto == null) {
			throw new BadRequestException(Messages.MESSAGES_BAD_REQUEST);
		}
	}
	
	public static void validateLocation (LocationDTO location) {
		validateString(location.getLatitude());
		validateString(location.getLongitude());
		validateString(location.getNomeBase());
	}
	
	//Validar cpfs - se tempo melhorar a lógica para validar uma lista de cpf
	public static  void validateTwoCpf(String cpf1, String cpf2) {
		
		if (!ValidateCpfCnpj.isCPF(cpf1)) {
			throw new BadRequestException(Messages.BAD_REQUEST_CPF);
		}
		
		if (!ValidateCpfCnpj.isCPF(cpf2)) {
			throw new BadRequestException(Messages.BAD_REQUEST_CPF);
		}
	
	}
	
	//Validar dados para salvar lider
	public static void validationSaveLeader(Leader lider, String senha) {
		
		Util.validateAge(lider.getIdade());
		
		//TODO Se der tempo passar senha pelo propertie pois o env só é recurso SPRING
		if ("".equals(senha) || senha == null || !senha.equals("f325n76DVG")) {
			throw new BadRequestException(Messages.LEADER_BAD_REQUEST_PSWD);
		}
		
		if (!ValidateCpfCnpj.isCPF(lider.getCpf())) {
			throw new BadRequestException(Messages.BAD_REQUEST_CPF + " " + lider.getCpf());
		}
		
		if (!Util.isValidateGenero(lider.getSexo())) {
			throw new BadRequestException(Messages.REBEL_GENRE);
		}
	}


}
