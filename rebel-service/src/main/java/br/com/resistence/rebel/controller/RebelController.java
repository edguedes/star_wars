package br.com.resistence.rebel.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.resistence.exception.BadRequestException;
import br.com.resistence.general.traitor.dto.TraitorDTO;
import br.com.resistence.general.traitor.model.Traitor;
import br.com.resistence.location.dto.LocationDTO;
import br.com.resistence.rebel.model.Rebel;
import br.com.resistence.rebel.service.IRebelService;
import br.com.resistence.util.Messages;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/rebels")
public class RebelController {

	//Log4j para acompanhar execução do código
	protected Logger logger = Logger.getLogger(RebelController.class.getName());

	private final IRebelService rebelService;

	public RebelController(IRebelService rebelService) {
		this.rebelService = rebelService;
	}
	
	@ApiOperation(value = "Request to sava Rebel ")
	@PostMapping(value = "/leader/{cpf}")
	public ResponseEntity<Rebel> create(@Valid @RequestBody Rebel rebelde, @PathVariable("cpf") String cpf, Errors errors) {
		logger.info("Request to save Rebel");

		//Capturar os erros de validações do controller
		if (errors.hasErrors()) {
			throw new BadRequestException(Messages.MESSAGES_BAD_REQUEST);
		}
		Rebel created = rebelService.create(rebelde, cpf);	
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}

	@GetMapping(value = "/id/{id}")
	public ResponseEntity<Rebel> getId(@PathVariable Long id) {
		logger.info("Request to get ID");

		Rebel result = rebelService.getOne(id);
		return new ResponseEntity<Rebel>(result, HttpStatus.OK);
	}

	@PutMapping()
	public ResponseEntity<Rebel> updateLocalition(@Valid @RequestBody LocationDTO locationDTO, Errors errors) {
		logger.info("Request to updateLocation");

		//Capturar os erros de validações do controller
		if (errors.hasErrors()) {
			throw new BadRequestException(Messages.MESSAGES_BAD_REQUEST);
		}

		Rebel result = rebelService.updateLocation(locationDTO);
		return new ResponseEntity<Rebel>(result, HttpStatus.OK);
	}

	@PostMapping(value = "/inform-traitors")
	public ResponseEntity<?> informTraitor(@Valid @RequestBody TraitorDTO traidorDTO, Errors errors) {
		logger.info("Request to inform traitor");

		//Capturar os erros de validações do controller
		if (errors.hasErrors()) {
			throw new BadRequestException(Messages.MESSAGES_BAD_REQUEST);
		}

		rebelService.informTraitor(traidorDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping()
	public ResponseEntity<List<Rebel>> getAll() {
		logger.info("Request all traitors");

		List<Rebel> result = rebelService.getAll();
		return new ResponseEntity<List<Rebel>>(result, HttpStatus.OK);
	}
	
	@PostMapping(value = "/traitor/{cpf}")
	public ResponseEntity<Rebel> setteTraitor(@PathVariable("cpf") String cpf) {
		logger.info("Request to save Traitors");
		Rebel created = rebelService.setteTraitor(cpf);	
		return new ResponseEntity<Rebel>(created, HttpStatus.OK);
	}
}
