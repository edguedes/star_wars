package br.com.resistence.rebel.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "TB_ITEMS")
public class Items implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_items")
	private Long id;

	@NotBlank
	@Size(max = 20)
	private String nome;

	@NotNull
	private int ponto;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Rebel rebelde;

	public Items() {
		
	}
	
	public Items(String nome, int ponto) {
		this.nome = nome;
		this.ponto = ponto;
	}

	public Rebel getRebelde() {
		return rebelde;
	}
	@JsonIgnore
	public void setRebelde(Rebel rebelde) {
		this.rebelde = rebelde;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPonto() {
		return ponto;
	}

	public void setPonto(int ponto) {
		this.ponto = ponto;
	}
	
}
