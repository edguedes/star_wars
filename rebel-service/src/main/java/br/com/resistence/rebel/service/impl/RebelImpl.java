package br.com.resistence.rebel.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.resistence.exception.BadRequestException;
import br.com.resistence.exception.ConflictException;
import br.com.resistence.exception.NotFoundException;
import br.com.resistence.general.traitor.dto.TraitorDTO;
import br.com.resistence.general.traitor.model.Traitor;
import br.com.resistence.general.traitor.repository.ITraitorRepository;
import br.com.resistence.leader.model.Leader;
import br.com.resistence.leader.repository.ILeaderRepository;
import br.com.resistence.location.dto.LocationDTO;
import br.com.resistence.location.model.Location;
import br.com.resistence.location.repository.ILocationRepository;
import br.com.resistence.rebel.model.Items;
import br.com.resistence.rebel.model.Rebel;
import br.com.resistence.rebel.model.dto.RebelBusinessDTO;
import br.com.resistence.rebel.repository.IRebelRepository;
import br.com.resistence.rebel.service.IRebelService;
import br.com.resistence.util.Messages;
import br.com.resistence.util.Util;
import br.com.resistence.util.ValidateCpfCnpj;

@Service
public class RebelImpl implements IRebelService{

	private final IRebelRepository rebelRepository;

	private final ILocationRepository locationRepository;

	private final ITraitorRepository traitorRepository;
	
	private final ILeaderRepository leaderRepository;

	public RebelImpl(IRebelRepository rebelRepository, ILocationRepository locationRepository,
			ITraitorRepository traitorRepository, ILeaderRepository leaderRepository) {
		this.rebelRepository = rebelRepository;
		this.locationRepository = locationRepository;
		this.traitorRepository = traitorRepository;
		this.leaderRepository = leaderRepository;
	}

	@Override
	/*
	 * Validar e salva rebeldes 
	 * */
	public Rebel create(Rebel rebelde, String cpf) {
		
		//Validar cpf
		Util.validateTwoCpf(rebelde.getCpf(), cpf);
		
		Optional<Leader> leaderOpt = leaderRepository.findByCpf(cpf);
		Optional<Rebel> rebelOpt = rebelRepository.findByCpf(rebelde.getCpf());
		List<Items> newItens = new ArrayList<Items>();
		
		//Validacao que impede rebelde duplicado
		if (rebelOpt.isPresent()) {
			throw new ConflictException(Messages.MESSAGES_REBEL_DUPLICATE);
		}
		
		//Rebelde so pode ser criado pelos lides
		if (leaderOpt.isPresent()) {
			
			//Validar sexo para evitar outras siglas
			if (!Util.isValidateGenero(rebelde.getSexo())) {
				throw new BadRequestException(Messages.REBEL_GENRE);
			}
			
			for(Items itens : rebelde.getItens()){
				
				//TODO validar itens pelo nome se tiver TEMPO validar pontos
				if (Util.isValidateItens(itens.getNome())) {
					newItens.add(itens);
				}
			}
			rebelde.setItens(newItens);
			rebelde.setIdade(Util.validateAge(rebelde.getIdade()));
			rebelde.setTipoRebelde(Messages.REBEL_TYPE_REBEL);
			rebelde.setCpf(rebelde.getCpf());
			rebelde.setLocalizacao(this.validadeLocation(rebelde));
			
			//remover caracteres especiais do nome
			rebelde.setNome(Util.removeSpecialCharacters(rebelde.getNome()));

			return rebelRepository.save(rebelde);
		}
		throw new ConflictException(Messages.MESSAGES_CONFLICT_REGISTER);
	}

	public void informTraitor(TraitorDTO traidorDTO) {
		Util.validateString(traidorDTO.getCpfInformante());
		Util.validateString(traidorDTO.getCpfTraidor());

		Optional<Rebel> informanteOpt = rebelRepository.findByCpf(traidorDTO.getCpfInformante());

		if(!informanteOpt.isPresent() ) {
			throw new NotFoundException(Messages.REBEL_NOT_FOUND);
		}

		Optional<Rebel> traidorOpt = rebelRepository.findByCpf(traidorDTO.getCpfTraidor());

		if(!traidorOpt.isPresent() ) {
			throw new NotFoundException(Messages.REBEL_NOT_FOUND_TRAITOR);
		}
		
		//Validar para que um rebelde não vote mais de uma vez na mesmo rebelde
		Traitor traidorSave = new Traitor(informanteOpt.get(), traidorOpt.get());
		long result = traitorRepository.votoTraitor(traidorSave.getInformante().getId(), traidorSave.getTraidor().getId());

		if (result >= 1) {
			throw new ConflictException(Messages.REBEL_CONFLICT_VOTO);
		}
		
		long qtdVotos = traitorRepository.findTraitors(traidorOpt.get().getId());
		
		if (qtdVotos > 2) {
			traidorOpt.get().setStatusTraidor(true);
			rebelRepository.save(traidorOpt.get());
		}
		traitorRepository.save(traidorSave);
		
	}
	
	/*
	 * Setar traidor pelo CPF
	 * */
	public Rebel setteTraitor(String cpfTraidor) {
		Util.validateString(cpfTraidor);

		Optional<Rebel> traidorOpt = rebelRepository.findByCpf(cpfTraidor);
		
		//Validar not found
		if(!traidorOpt.isPresent() ) {
			throw new NotFoundException(Messages.REBEL_NOT_FOUND_TRAITOR);
		}
		
		//Verificar qtdVotos de traidores
		long result = traitorRepository.findTraitors(traidorOpt.get().getId());
		
		//Setar traidor no terceiro voto 
		if (result > 2) {
			traidorOpt.get().setStatusTraidor(true);
			return rebelRepository.save(traidorOpt.get());
		}
		return rebelRepository.save(traidorOpt.get());
	}

	@Override
	public Rebel getOne(Long id) {
		if (id == null) {
			throw new BadRequestException("ID inválido");
		}
		Optional<Rebel> resultOpt = rebelRepository.findById(id);

		if (resultOpt.isPresent()) {
			return resultOpt.get();
		}
		throw new NotFoundException(Messages.REBEL_NOT_FOUND);
	}

	private Location validadeLocation(Rebel rebelde) {

		if (rebelde.getLocalizacao().getLatitude() != null &&
				rebelde.getLocalizacao().getLongitude() != null) {

			Optional<Location> locationOpt = locationRepository.findLocation(rebelde.getLocalizacao().getLatitude(),
					rebelde.getLocalizacao().getLongitude());

			if (locationOpt.isPresent()) {
				return locationOpt.get();
			}
			Location localizacao =  new Location(rebelde.getLocalizacao().getNomeBase(),
					rebelde.getLocalizacao().getLatitude(),
					rebelde.getLocalizacao().getLongitude());
			return locationRepository.save(localizacao);
		}
		throw new BadRequestException(Messages.MESSAGES_BAD_REQUEST_LOCATION);
	}

	@Override
	public Rebel updateLocation(LocationDTO locationDTO) {
		Util.validateLocation(locationDTO);
		Location location = locationDTO.toEntity();

		Optional<Rebel> rebeldeOpt = rebelRepository.findByCpf(locationDTO.getCpf());

		if (rebeldeOpt.isPresent()) {
			Optional<Location> locationOpt = locationRepository.findLocation(location.getLatitude(), 
					location.getLongitude());

			if (locationOpt.isPresent()) {
				rebeldeOpt.get().setLocalizacao(locationOpt.get());
				return rebelRepository.save(rebeldeOpt.get());
			}

			Location result = locationRepository.save(location);
			rebeldeOpt.get().setLocalizacao(result);
			return rebelRepository.save(rebeldeOpt.get());

		}throw new NotFoundException(Messages.REBEL_NOT_FOUND);

	}

	@Override
	public List<Rebel> getAll() {

		List<Rebel> listRebel = rebelRepository.findByStatusTraidor(false);
		return listRebel;
	}

}
