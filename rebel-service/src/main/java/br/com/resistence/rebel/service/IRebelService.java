 package br.com.resistence.rebel.service;

import java.util.List;

import br.com.resistence.general.traitor.dto.TraitorDTO;
import br.com.resistence.location.dto.LocationDTO;
import br.com.resistence.rebel.model.Rebel;

public interface IRebelService {
	
	Rebel create(Rebel rebelde, String cpf);
	
	Rebel getOne(Long id);
	
	Rebel updateLocation(LocationDTO locationDTO);
	
	void informTraitor(TraitorDTO traidorDTO);

	List<Rebel> getAll();

	Rebel setteTraitor(String cpf);
	
}
