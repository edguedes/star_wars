package br.com.resistence.rebel.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.resistence.location.model.Location;
import br.com.resistence.reference.Person;

@Entity
@Table(name = "TB_REBEL")
public class Rebel extends Person implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_rebel")
	private Long id;

	@NotBlank
	@Size(max = 100)
	private String nome;
	
	@NotBlank
	@Size(min = 11, max = 11)
	private String cpf;
	
	@NotNull
	private Long idade;
	
	@Column(columnDefinition="tinyint(0) default 0")
	private boolean statusTraidor;
	
	@NotBlank
	@Size(max = 1)
	private String sexo;
	
	private boolean tipoRebelde;
	
	@OneToMany(cascade=CascadeType.ALL)
	@Valid
	List<Items> itens = new ArrayList<Items>();
	
	@OneToOne(cascade = CascadeType.ALL)
	@Valid
	private Location localizacao;

	public Rebel() {
		
	}

	public Rebel(@NotBlank @Size(max = 100) String nome, String cpf, @NotNull Long idade, boolean statusTraidor,
			@NotBlank @Size(max = 1) String sexo, boolean tipoRebelde, @Valid List<Items> itens,
			@Valid Location localizacao) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.idade = idade;
		this.statusTraidor = statusTraidor;
		this.sexo = sexo;
		this.tipoRebelde = tipoRebelde;
		this.itens = itens;
		this.localizacao = localizacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Long getIdade() {
		return idade;
	}

	public void setIdade(Long idade) {
		this.idade = idade;
	}

	public boolean isStatusTraidor() {
		return statusTraidor;
	}

	public void setStatusTraidor(boolean statusTraidor) {
		this.statusTraidor = statusTraidor;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public boolean isTipoRebelde() {
		return tipoRebelde;
	}

	public void setTipoRebelde(boolean tipoRebelde) {
		this.tipoRebelde = tipoRebelde;
	}

	public List<Items> getItens() {
		return itens;
	}

	public void setItens(List<Items> itens) {
		this.itens = itens;
	}

	public Location getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(Location localizacao) {
		this.localizacao = localizacao;
	}
	
}
