package br.com.resistence.rebel.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.resistence.rebel.model.Rebel;

@Repository
public interface IRebelRepository extends JpaRepository<Rebel, Long>{

 Optional<Rebel> findByCpf(String cpf);
 
 List<Rebel> findByStatusTraidor(boolean status);
 
 
}
