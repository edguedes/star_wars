package br.com.resistence.rebel.model.dto;

import javax.validation.constraints.NotBlank;

public class RebelBusinessDTO {
	
	@NotBlank
	private String cpfRebelde1;
	
	@NotBlank
	private String cpfRebelde2;

	public String getCpfRebelde1() {
		return cpfRebelde1;
	}

	public void setCpfRebelde1(String cpfRebelde1) {
		this.cpfRebelde1 = cpfRebelde1;
	}

	public String getCpfRebelde2() {
		return cpfRebelde2;
	}

	public void setCpfRebelde2(String cpfRebelde2) {
		this.cpfRebelde2 = cpfRebelde2;
	}
	
}
