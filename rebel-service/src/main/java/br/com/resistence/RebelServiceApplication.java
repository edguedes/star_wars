package br.com.resistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RebelServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RebelServiceApplication.class, args);
	}

}
