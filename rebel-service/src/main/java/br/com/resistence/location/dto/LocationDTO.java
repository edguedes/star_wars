package br.com.resistence.location.dto;

import javax.validation.constraints.NotBlank;

import br.com.resistence.location.model.Location;

public class LocationDTO {

	@NotBlank
	private String nomeBase;
	
	@NotBlank
	private String latitude;

	@NotBlank
	private String longitude;
	
	@NotBlank
	private String cpf;
	

	public String getNomeBase() {
		return nomeBase;
	}

	public void setNomeBase(String nomeBase) {
		this.nomeBase = nomeBase;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public Location toEntity() {
		Location location = new Location();
		location.setLatitude(this.latitude);
		location.setLongitude(this.longitude);
		location.setNomeBase(this.nomeBase);
		return location;
	}
}
