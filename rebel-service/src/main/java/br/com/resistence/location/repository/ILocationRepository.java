package br.com.resistence.location.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.resistence.location.model.Location;

@Repository
public interface ILocationRepository extends JpaRepository<Location, Long>{

	@Transactional
	@Query(value = "select * FROM tb_location l where l.latitude = :latitude and l.longitude = :longitude", nativeQuery = true)
	Optional<Location> findLocation(@Param("latitude") String latitude, @Param("longitude") String longitude);
}
