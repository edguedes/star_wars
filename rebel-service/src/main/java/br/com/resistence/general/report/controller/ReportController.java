package br.com.resistence.general.report.controller;

import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.resistence.general.report.model.ReportRebel;
import br.com.resistence.general.report.service.IReportService;
import br.com.resistence.rebel.controller.RebelController;

@RestController
@RequestMapping("/reports")
public class ReportController {

	//Log4j para acompanhar execução do código
	protected Logger logger = Logger.getLogger(RebelController.class.getName());

	private final IReportService reportService;

	public ReportController(IReportService reportService) {
		this.reportService = reportService;
	}
	
	@GetMapping()
	public ResponseEntity<ReportRebel> getReport() {
		logger.info("Request to Report");

		ReportRebel result = reportService.mountReport();
		return new ResponseEntity<ReportRebel>(result, HttpStatus.OK);
	}
}
