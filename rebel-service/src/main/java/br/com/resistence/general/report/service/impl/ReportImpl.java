package br.com.resistence.general.report.service.impl;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import br.com.resistence.general.report.model.ReportRebel;
import br.com.resistence.general.report.service.IReportService;
import br.com.resistence.rebel.model.Rebel;
import br.com.resistence.rebel.repository.IRebelRepository;
import br.com.resistence.util.Messages;

@Service
public class ReportImpl  implements IReportService{

	private final IRebelRepository rebelRepository;

	public ReportImpl(IRebelRepository rebelRepository) {
		this.rebelRepository = rebelRepository;
	}

	//Formatar a casas deciamais para doi digitos
	DecimalFormat formatador = new DecimalFormat("0.00");

	@Override
	/*
	 * Calcula e retorna o percentual de traidores 
	 * */
	public String percentTraitor() {
		List<Rebel> traidores = rebelRepository.findByStatusTraidor(true);
		List<Rebel> rebeldes = rebelRepository.findByStatusTraidor(false);

		double totalRebeldes = rebeldes.size();
		double totalTraidores = traidores.size();


		if (totalTraidores == 0)
			return "0.00 %";

		double percent = (totalTraidores / totalRebeldes) * 100;

		DecimalFormat formatador = new DecimalFormat("0.00");
		String result = formatador.format(percent);

		return result + " %";

	}

	@Override
	/*
	 * Calcula e retorna o percentual de rebeldes 
	 * */
	public String percentRebel() {
		List<Rebel> traidores = rebelRepository.findByStatusTraidor(true);
		List<Rebel> rebeldes = rebelRepository.findByStatusTraidor(false);

		double totalRebeldes = rebeldes.size();
		double totalTraidores = traidores.size();

		if (totalRebeldes == 0)
			return "0";

		double percent = 100 - ((totalTraidores / totalRebeldes) * 100);
		String result = formatador.format(percent);

		return result + " %";

	}

	@Override
	/*
	 * Calcula os pontos perdidos pelos traidores 
	 * */
	public double lostPointsTraitors() {
		List<Rebel> traidores = rebelRepository.findByStatusTraidor(true);

		double totalTraidores = traidores.size();
		double pontosPerdidos = 0;
		int i = 0;

		if (totalTraidores == 0)
			return 0;

		for (Rebel traidor : traidores ) {
			for ( int j = 0 ; j < traidor.getItens().size(); j++) {
				pontosPerdidos += traidor.getItens().get(i).getPonto();
				i++;
			}
		}

		return pontosPerdidos;
	}

	@Override
	/*
	 * Calcula e retorna a lista de itens calculando a media 
	 * */
	public Map<String,String> listItems() {
		List<Rebel> rebeldes = rebelRepository.findByStatusTraidor(false);
		Map<String,String> lista = new HashMap<String,String>();

		double totalRebeldes = rebeldes.size();
		int qtdArma = 0;
		int qtdMunicao = 0;
		int qtdAgua = 0;
		int qtdComida = 0;
		int i = 0;

		/*
		 * Uso do FOR FOR para faser leitura de lista dentro de lista 
		 * */																																																																																																																																																																																																																																																																																										for (Rebel r : rebeldes) {
			 for (Rebel rebelde : rebeldes) {
				 for ( int j = 0 ; j < rebelde.getItens().size(); j++) {
					 if (Messages.ITEMS_MUNICAO.equals(rebelde.getItens().get(j).getNome())) {
						 qtdMunicao ++; 
					 }
					 if (Messages.ITEMS_ARMA.equals(rebelde.getItens().get(j).getNome())) {
						 qtdArma ++; 
					 }
					 if (Messages.ITEMS_COMIDA.equals(rebelde.getItens().get(j).getNome())) {
						 qtdAgua ++; 
					 }
					 if (Messages.ITEMS_AGUA.equals(rebelde.getItens().get(j).getNome())) {
						 qtdComida ++; 
					 }
				 }
				 i++;
			 }

			 // IF para tratar a resposta por dividir valor por zero 
			 if  (qtdArma == 0) {
				 lista.put(Messages.ITEMS_ARMA, "0.00");
			 }else {
				 double valor = qtdArma / totalRebeldes;
				 String result = formatador.format(valor);
				 lista.put(Messages.ITEMS_ARMA, result);
			 }
			 if  (qtdMunicao == 0) {
				 lista.put(Messages.ITEMS_MUNICAO, "0.00");
			 }else {
				 double valor = qtdMunicao / totalRebeldes;
				 String result = formatador.format(valor);
				 lista.put(Messages.ITEMS_MUNICAO, result);
			 }
			 if  (qtdAgua == 0) {
				 lista.put(Messages.ITEMS_COMIDA, "0.00");
			 }else {
				 double valor = qtdAgua / totalRebeldes;
				 String result = formatador.format(valor);
				 lista.put(Messages.ITEMS_COMIDA, result);
			 }
			 if  (qtdAgua == 0) {
				 lista.put(Messages.ITEMS_AGUA, "0.00");
			 }else {
				 double valor = qtdComida / totalRebeldes;
				 String result = formatador.format(valor);
				 lista.put(Messages.ITEMS_AGUA, result);
			 }
		 }
		 return lista;
	}

	/*
	 * Montagem do relatorio 
	 * */
	public ReportRebel mountReport() {
		ReportRebel relatorio = new ReportRebel();

		relatorio.setPercentualRebeldes(percentRebel());
		relatorio.setPercentualTraidores(percentTraitor());
		relatorio.setMediaItensRebeldes(listItems());
		relatorio.setPontosPerdidosTraidores(lostPointsTraitors());

		return relatorio;
	}

}
