package br.com.resistence.general.negotiation;

import java.util.List;

import br.com.resistence.rebel.model.Rebel;

public interface INegotiationRebel {

	List<Rebel> realizarNegociacao(String matriculaRebelde1, String matriculaRebelde12);

}
