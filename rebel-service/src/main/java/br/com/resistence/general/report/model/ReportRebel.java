package br.com.resistence.general.report.model;

import java.util.HashMap;
import java.util.Map;

public class ReportRebel {

	private String percentualTraidores;
	
	private String percentualRebeldes;
	
	Map<String,String> mediaItensRebeldes = new HashMap<String,String>();
	
	private double pontosPerdidosTraidores;

	public String getPercentualTraidores() {
		return percentualTraidores;
	}

	public void setPercentualTraidores(String percentualTraidores) {
		this.percentualTraidores = percentualTraidores;
	}

	public String getPercentualRebeldes() {
		return percentualRebeldes;
	}

	public void setPercentualRebeldes(String percentualRebeldes) {
		this.percentualRebeldes = percentualRebeldes;
	}

	public Map<String, String> getMediaItensRebeldes() {
		return mediaItensRebeldes;
	}

	public void setMediaItensRebeldes(Map<String, String> mediaItensRebeldes) {
		this.mediaItensRebeldes = mediaItensRebeldes;
	}

	public double getPontosPerdidosTraidores() {
		return pontosPerdidosTraidores;
	}

	public void setPontosPerdidosTraidores(double pontosPerdidosTraidores) {
		this.pontosPerdidosTraidores = pontosPerdidosTraidores;
	}
	
}
