package br.com.resistence.general.traitor.dto;

import javax.validation.constraints.NotBlank;

public class TraitorDTO {

	@NotBlank
	private String cpfInformante;
	
	@NotBlank
	private String cpfTraidor;
	
	public TraitorDTO() {
		
	}

	public String getCpfInformante() {
		return cpfInformante;
	}

	public void setCpfInformante(String cpfInformante) {
		this.cpfInformante = cpfInformante;
	}

	public String getCpfTraidor() {
		return cpfTraidor;
	}

	public void setCpfTraidor(String cpfTraidor) {
		this.cpfTraidor = cpfTraidor;
	}
	
}
