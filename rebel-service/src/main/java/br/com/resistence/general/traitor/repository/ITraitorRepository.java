package br.com.resistence.general.traitor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.resistence.general.traitor.model.Traitor;
import br.com.resistence.rebel.model.Rebel;

@Repository
public interface ITraitorRepository extends JpaRepository<Traitor, Long>{

	@Transactional
	@Query(value = "select COUNT(t.traidor_id_rebel) FROM tb_traitor t where t.traidor_id_rebel = :id", nativeQuery = true)
	long findTraitors(@Param("id") Long id);

	@Query(value = "SELECT * FROM tb_rebel r JOIN tb_traitor t "
			+ "ON (r.id_rebel = t.traidor_id_rebel) "
			+ "WHERE r.cpf = :cpf", nativeQuery = true)
	List<Rebel> findListTraitors(@Param("cpf") String cpf);

	@Transactional
	@Query(value = "select COUNT(*) FROM tb_traitor t where t.informante_id_rebel = :id_informante and t.traidor_id_rebel = :id_traidor ", nativeQuery = true)
	long votoTraitor(@Param("id_informante") Long id_informante, @Param("id_traidor") Long id_traidor);

	@Query(value = "SELECT * FROM tb_rebel r JOIN tb_traitor t "
			+ "ON (r.id_rebel = t.traidor_id_rebel) "
			+ "WHERE r.status_traidor = :status", nativeQuery = true)
	long findTraitorStatus(@Param("status") boolean status);

}
