package br.com.resistence.general.negotiation.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.resistence.exception.ConflictException;
import br.com.resistence.exception.NotFoundException;
import br.com.resistence.general.negotiation.INegotiationRebel;
import br.com.resistence.rebel.model.Items;
import br.com.resistence.rebel.model.Rebel;
import br.com.resistence.rebel.repository.IRebelRepository;
import br.com.resistence.util.Messages;

@Service
public class NegotiationRebelImpl implements INegotiationRebel {

	private final IRebelRepository rebelRepository;

	public NegotiationRebelImpl(IRebelRepository rebelRepository) {
		this.rebelRepository = rebelRepository;
	}

	@Override
	/*
	 * Metodo valida e realiza troca de itens entre rebeldes
	 * */
	public List<Rebel> realizarNegociacao(String cpfRebelde1, String cpfRebelde2) {

		Optional<Rebel> rebeldeOpt1 = rebelRepository.findByCpf(cpfRebelde1);
		Optional<Rebel> rebeldeOpt2 = rebelRepository.findByCpf(cpfRebelde2);
		
		//Validar presenca de rebel e se eh traidor.
		//TODO Se tempo validar traidor no select
		if (!rebeldeOpt1.isPresent() || rebeldeOpt1.get().isStatusTraidor()) {
			throw new NotFoundException(Messages.NEGOTIATION_NOT_FOUND + " " + cpfRebelde1);
		}
		
		if (!rebeldeOpt2.isPresent() || rebeldeOpt1.get().isStatusTraidor()) {
			throw new NotFoundException(Messages.NEGOTIATION_NOT_FOUND + " " + cpfRebelde2);
		}
		
		List<Items> listItensRebelde1 = new ArrayList<>();
		List<Items> listItensRebelde2 = new ArrayList<>();
		listItensRebelde1 = rebeldeOpt1.get().getItens();
		listItensRebelde2 = rebeldeOpt2.get().getItens();
		
		int pontosRebelde1 = 0;
		int pontosRebelde2 = 0;
		
		for (Items t : listItensRebelde1) {
			pontosRebelde1 += t.getPonto();
		}
		
		for (Items t : listItensRebelde2) {
			pontosRebelde2 += t.getPonto();
		}
		
		//Valida se pontos iguais
		if (pontosRebelde1 == pontosRebelde2) {
			rebeldeOpt1.get().setItens(listItensRebelde2);
			rebeldeOpt2.get().setItens(listItensRebelde1);
			
			List<Rebel> rebeldes = new ArrayList<>();
			rebeldes.add(rebelRepository.save(rebeldeOpt1.get()));
			rebeldes.add(rebelRepository.save(rebeldeOpt2.get()));
			return rebeldes;	

		}throw new ConflictException(Messages.NEGOTIATION_CONFLICT_POINT);

	}


}
