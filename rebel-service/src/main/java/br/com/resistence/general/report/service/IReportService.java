package br.com.resistence.general.report.service;

import java.util.Map;

import br.com.resistence.general.report.model.ReportRebel;

public interface IReportService {
	
	String percentTraitor();

	String percentRebel();

	Map<String, String> listItems();

	double lostPointsTraitors();

	ReportRebel mountReport();

	

	
}
