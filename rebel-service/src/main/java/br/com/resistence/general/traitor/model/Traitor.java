package br.com.resistence.general.traitor.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.resistence.rebel.model.Rebel;

@Entity
@Table(name = "TB_TRAITOR")
public class Traitor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_traitor")
	private Long id;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Rebel informante;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Rebel traidor;

	public Traitor(Rebel informante, Rebel traidor) {
		super();
		this.informante = informante;
		this.traidor = traidor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Rebel getInformante() {
		return informante;
	}

	public void setInformante(Rebel informante) {
		this.informante = informante;
	}

	public Rebel getTraidor() {
		return traidor;
	}

	public void setTraidor(Rebel traidor) {
		this.traidor = traidor;
	}

}
