package br.com.resistence.general.negotiation.service.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.resistence.exception.BadRequestException;
import br.com.resistence.general.negotiation.INegotiationRebel;
import br.com.resistence.rebel.controller.RebelController;
import br.com.resistence.rebel.model.Rebel;
import br.com.resistence.rebel.model.dto.RebelBusinessDTO;
import br.com.resistence.util.Messages;

@RestController
@RequestMapping("/business")
public class NegotiationController {

	//Log4j para acompanhar execução do código
	protected Logger logger = Logger.getLogger(RebelController.class.getName());

	private final INegotiationRebel negotiateService;

	public NegotiationController(INegotiationRebel negotiateService) {
		this.negotiateService = negotiateService;
	}
	
	/*
	 * Realizar troca entre rebeldes
	 * */
	@PostMapping()
	public ResponseEntity<List<Rebel>> getReport(@Valid @RequestBody RebelBusinessDTO rebeldeDTO, Errors errors) {
		logger.info("Request to Business");

		//Capturar os erros de validações do controller
		if (errors.hasErrors()) {
			throw new BadRequestException(Messages.MESSAGES_BAD_REQUEST);
		}

		List<Rebel> result = negotiateService.realizarNegociacao(rebeldeDTO.getCpfRebelde1(), rebeldeDTO.getCpfRebelde2());
		return new ResponseEntity<List<Rebel>>(result, HttpStatus.OK);
	}
}
