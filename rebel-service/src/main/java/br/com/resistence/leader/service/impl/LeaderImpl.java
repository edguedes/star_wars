package br.com.resistence.leader.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import br.com.resistence.exception.BadRequestException;
import br.com.resistence.exception.ConflictException;
import br.com.resistence.exception.NotFoundException;
import br.com.resistence.leader.model.Leader;
import br.com.resistence.leader.repository.ILeaderRepository;
import br.com.resistence.leader.service.ILeaderService;
import br.com.resistence.util.Messages;
import br.com.resistence.util.Util;
import br.com.resistence.util.ValidateCpfCnpj;

@Service
public class LeaderImpl implements ILeaderService{
	
	private final ILeaderRepository leaderRepository;

	public LeaderImpl(ILeaderRepository leaderRepository) {
		this.leaderRepository = leaderRepository;
	}

	
	@Override
	public Leader create(Leader lider, String senha) {
		
		Util.validationSaveLeader(lider, senha);
		Optional<Leader> liderOpt = leaderRepository.findByCpf(lider.getCpf());
		
		if (liderOpt.isPresent()) {
			throw new ConflictException(Messages.LEADER_CONFLICT_REPEATED + " " + lider.getCpf());
		}
		
		lider.setNome(Util.removeSpecialCharacters(lider.getNome()));
		return leaderRepository.save(lider);
	}

	@Override
	public Leader getOne(Long id) {
		
		Optional<Leader> lider = leaderRepository.findById(id);
		
		if (!lider.isPresent()) {
			throw new NotFoundException(Messages.LEADER_NOT_FOUND + " " + id);
		}
		
		return lider.get();
	}

	@Override
	public List<Leader> getAll() {
		return leaderRepository.findAll();
	}

	@Override
	public Leader getCpf(String cpf) {
		
		if (!ValidateCpfCnpj.isCPF(cpf)) {
			throw new BadRequestException(Messages.BAD_REQUEST_CPF + " " + cpf);
		}
		Optional<Leader> lider = leaderRepository.findByCpf(cpf);
		
		if (!lider.isPresent()) {
			throw new NotFoundException(Messages.LEADER_NOT_FOUND + " " + cpf);
		}
		return lider.get();
	}
	
}
