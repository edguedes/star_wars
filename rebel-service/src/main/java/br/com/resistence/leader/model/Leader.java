package br.com.resistence.leader.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.resistence.reference.Person;

@Entity
@Table(name = "TB_LEADER")
public class Leader extends Person implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_leader")
	private Long id;

	@NotBlank
	@Size(max = 100)
	private String nome;

	@NotBlank
	@Size(min = 11, max = 11)
	private String cpf;
	
	@NotNull
	private Long idade;
	
	@NotBlank
	@Size(max = 1)
	private String sexo;
	

	public Leader() {
		super();
	}

	public Leader(Long id, @NotBlank @Size(max = 100) String nome, @NotBlank @Size(min = 11, max = 11) String cpf,
			@NotNull Long idade, @NotBlank @Size(max = 1) String sexo) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.idade = idade;
		this.sexo = sexo;
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Long getIdade() {
		return idade;
	}

	public void setIdade(Long idade) {
		this.idade = idade;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
}
