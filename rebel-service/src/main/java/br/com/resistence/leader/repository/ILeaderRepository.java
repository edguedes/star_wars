package br.com.resistence.leader.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.resistence.leader.model.Leader;
import br.com.resistence.rebel.model.Rebel;

public interface ILeaderRepository extends JpaRepository<Leader, Long>{

	Optional<Leader> findByCpf(String cpf);
}
