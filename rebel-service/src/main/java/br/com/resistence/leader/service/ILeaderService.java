package br.com.resistence.leader.service;

import java.util.List;

import br.com.resistence.leader.model.Leader;

public interface ILeaderService {
	
	Leader create(Leader rebelde, String senha);
	
	Leader getOne(Long id);
	
	List<Leader> getAll();

	Leader getCpf(String cpf);
}
