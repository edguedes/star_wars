package br.com.resistence.leader.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.resistence.exception.BadRequestException;
import br.com.resistence.leader.model.Leader;
import br.com.resistence.leader.service.ILeaderService;
import br.com.resistence.rebel.controller.RebelController;
import br.com.resistence.util.Messages;

@RestController
@RequestMapping("/leaders")
public class LeaderController {

	//Log4j para acompanhar execução do código
	protected Logger logger = Logger.getLogger(RebelController.class.getName());

	private final ILeaderService leaderService;

	public LeaderController(ILeaderService leaderService) {
		this.leaderService = leaderService;
	}

	@PostMapping(value = "/{senha}")
	public ResponseEntity<Leader> create(@Valid @RequestBody Leader lider, @PathVariable("senha") String senha, Errors errors) {
		logger.info("Request to save Leader");

		//Capturar os erros de validações do controller
		if (errors.hasErrors()) {
			throw new BadRequestException(Messages.MESSAGES_BAD_REQUEST);
		}

		Leader created = leaderService.create(lider, senha);	
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Leader> getId(@Valid @PathVariable Long id) {
		logger.info("Request to get ID" + id + "Leader");

		Leader result = leaderService.getOne(id);
		return new ResponseEntity<Leader>(result, HttpStatus.OK);
	}

	@GetMapping()
	public ResponseEntity<List<Leader>> getAll() {
		logger.info("Request all leaders");

		List<Leader> result = leaderService.getAll();
		return new ResponseEntity<List<Leader>>(result, HttpStatus.OK);
	}

	@GetMapping(value = "/register/{cpf}")
	public ResponseEntity<Leader> getCpf(@Valid @PathVariable String cpf) {
		logger.info("Request to CPF" + cpf + "Leader");

		
		Leader created = leaderService.getCpf(cpf);	
		return new ResponseEntity<Leader>(created, HttpStatus.OK);
	}

}
