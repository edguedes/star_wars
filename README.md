**Star Wars Resistence**

O império continua sua luta incessante de dominar a galáxia, tentando ao máximo expandir seu território e eliminar rebeldes.
Você, como um soldado de resistência, foi designado para desenvolver um sistema para compartilhar recursos entre rebeldes.

Star Wars Resistence  - é uma API que tem a finalidade de genrenciamento de Rebeldes. Como: 

* 	Cadastrar rebelde
* 	Informar localização
* 	Informar traidores
* 	Efetuar negociação
* 	Gerar relatórios


**Autenticação**

A API não presica de autenticação prévia. Apenas seguir as regras definida na SLA.

**Error Codes**
Pricipais status usados na API

**201 – Created**
Confirmação de salvamento de um objeto

**200 – Ok**
Confirmação de ação

**400 – Bad Request**
Dados enviados de forma incorreta ou fora do padrão

**409 – Conflict**
Conflitos gerados na quebra da regra de negócio

**404 – Not Found**
Dados não encontrados na base de dados

**Tecnologias**
*Java *Spring boot *Spring Data *Hibernate
*MySQL *Maven *Docker


**USABILIDADE**

**CRUD de Líder**

> A lógica da API precisa do cadastro de um lider que apenas ele tem autorização para cadastrar rebeldes. 

* **POST** localhost:8081/leaders/f325n76DVG
*  Senha (f325n76DVG) secreta para cadastrar um líder. Poucos conhecem sua existência.

```
{
	"nome" : "Lorena Jennifer Porto",
	"cpf" : "78793732686",
	"idade" : 39,
	"sexo" : "F"
	
}
```
**Busca de lideres **

*  **GET** localhost:8081/leaders/ - Lista todos os lideres
*  **GET** localhost:8081/leaders/register/78793732686 - Busca por CPF
*  **GET** localhost:8081/rebels/id/11 - Busca por ID


**Gerenciador de Rebelde**

> A lógica da API precisa do CPF de um lider pois apenas ele tem autorização para cadastrar rebeldes.
    

* **POST** localhost:8081/rebels/leader/78793732686
*  CPF (78793732686) de um líder para cadastrar um rebelde. 

```
{
	"nome":"Sueli Tereza Esther Ferreira",
	"idade": 37,
	"cpf" : "04384218257",
	"sexo" : "F", 
	"itens": [{
		"nome" : "Comida",
		"ponto": 1
	},
	{
		"nome" : "Comida",
		"ponto": 1
	},
	{
		"nome" : "Comida",
		"ponto": 1
	}
	,{
		"nome" : "Comida",
		"ponto": 1
	}
	],
	"localizacao": {
		"nomeBase" : "Casa Rosa", 
		"latitude": "231",
		"longitude" : "001"
	}
	
}
```

* **PUT** localhost:8081/rebels
*  Atualizar localição

```
{
	"nomeBase" : "Aruripe",
	"latitude" : "187", 
	"longitude" : "500",
	"cpf" : "96028281964"
}
```

* **POST** localhost:8081/rebels/inform-traitors
*  CPF de um rebelde Informante e do Rebelde Traidor

```
{
	"cpfInformante" : "84067550839",
	"cpfTraidor": "86595727773"
}
```

* **POST** localhost:8081/business
*  CPF's de rebeldes que desejam trocar mercadorias

```
{
	"cpfRebeçde1" : "86595727773", 
	"cpfRebelde2": "29457196066"
}
```
* **GET** localhost:8081/reports
*  Relatório gestão que permite crescimento
```
{
    "percentualTraidores": "20,00 %",
    "percentualRebeldes": "80,00 %",
    "mediaItensRebeldes": {
        "Comida": "6,00",
        "Municao": "0.00",
        "Arma": "1,00",
        "Agua": "3,00"
    },
    "pontosPerdidosTraidores": 6.0
}
```