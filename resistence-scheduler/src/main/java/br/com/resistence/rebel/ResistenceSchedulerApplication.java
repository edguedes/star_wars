package br.com.resistence.rebel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResistenceSchedulerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResistenceSchedulerApplication.class, args);
	}

}
