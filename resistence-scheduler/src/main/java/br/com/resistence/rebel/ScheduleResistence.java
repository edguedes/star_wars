package br.com.resistence.rebel;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class ScheduleResistence {
	
	//Enviar Logs via console para acompanhamento do Start e End do service
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private static final String TIME_ZONE = "America/Sao_Paulo";
    
    @Scheduled(cron = "0 0 12 * * ?")
    @Scheduled(cron = "0 0 16 * * ?")
    public void reportCurrentTime2() {
    	System.out.println("start - " + dateFormat.format(new Date()));
    	// Cahmar serviço para varrer traidores
    	System.out.println("the end - " + dateFormat.format(new Date()));
    }
}
